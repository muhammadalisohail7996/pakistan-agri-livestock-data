# Pakistan-Agri-Livestock-Data

**Livestock and Agriculture Data Repository**

**Agriculture Data:**

Explore the dynamic landscape of agricultural trends with our data. Overall, agricultural growth has been variable, peaking at 4.40% in 2021–22. Crops witnessed a notable decline in 2018–19 (-4.38%), followed by a robust recovery and substantial growth in subsequent years. Livestock consistently demonstrated positive growth, ranging from 2.38% to 3.65%. Forestry experienced fluctuations, including a significant increase in 2021–22 (6.13%), while the fishing sector remained relatively stable, ranging from 0.35% to 1.57%.

**Livestock Data:**

Delve into the specifics of our livestock data. Cattle numbers increased from 49.6 million in 2019–20 to 53.4 million in 2021–22, showcasing a positive trajectory. Buffaloes witnessed a rise from 41.2 million in 2019–20 to 43.7 million in 2021–22. Sheep exhibited a slight increase from 31.2 million to 31.9 million during the same period. Goats experienced notable growth, increasing from 78.2 million in 2019–20 to 82.5 million in 2021–22. Asses saw a small rise from 5.5 million in 2019–20 to 5.7 million in 2021–22. Explore the nuances of livestock dynamics with our comprehensive data repository.

